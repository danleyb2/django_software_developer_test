- *Invalid date '10/70/2015' changed to '10/7/2015'*  
- *the data is parsed in csv format*


# Question one (Python test)
Using pure python write an application that reads this data and performs the following analysis
Find the farmers with the highest and lowest Total biomass Fwt (kg)
Sorts the list in ascending order by Harvesting date
Find the average No of plants harvest

## Solution
The processing logic is implemented here [`question1/app.py`](question1/app.py)  
The results will be output after executing this scripts

1. Find the farmers with the highest and lowest Total biomass Fwt (kg)  
    [`question1/get_highest_and_lowest_total_biomass_fwt.py`](question1/get_highest_and_lowest_total_biomass_fwt.py)
2. Sorts the list in ascending order by Harvesting date  
    [`question1/order_harvesting_dates.py`](question1/order_harvesting_dates.py)
3. Find the average No of plants harvest  
    [`question1/avg_no_of_plants_harvests.py`](question1/avg_no_of_plants_harvests.py)


# Question two (SQL test)
Load the data from question one into a postgres database and using sql, 
perform the same analysis by creating views of the same.

## Solution
using a database named `question2`(or any other name),  
run the sql script [`question2/question2.sql`](question2/question2.sql),(can using this command `psql -U postgres -d question2 -a -f question2/question2.sql`)

this will do the following  
- [Create sample Data table](https://gitlab.com/danleyb2/django_software_developer_test/blob/master/question2/question2.sql#L4)
- [Insert Sample Data](https://gitlab.com/danleyb2/django_software_developer_test/blob/master/question2/question2.sql#L32)
- Create views for
    1. Find the farmers with the highest and lowest Total biomass Fwt (kg)  
        [highest_and_lowest_total_biomass_fwt](https://gitlab.com/danleyb2/django_software_developer_test/blob/master/question2/question2.sql#L76)
    2. Sorts the list in ascending order by Harvesting date 
        [ordered_harvesting_dates](https://gitlab.com/danleyb2/django_software_developer_test/blob/master/question2/question2.sql#L90)
    3. Find the average No of plants harvest 
        [no_of_plants_harvests_avg](https://gitlab.com/danleyb2/django_software_developer_test/blob/master/question2/question2.sql#L96)
     
Now Query the data from the views
```postgresql
    SELECT * FROM highest_and_lowest_total_biomass_fwt;
    SELECT * FROM ordered_harvesting_dates;
    SELECT * FROM no_of_plants_harvests_avg;
```

# Question three (Django test)
Using django create a simple web app that allows a user to login/signup and fill out a feedback form.  
The feedback data should only be viewed by an admin/superuser who can then export the data to into a csv.  

# The required fields are
Name (automatically loaded from user object)  
Phone number (validation is key here)  
Neighbourhood - A drop down with a list of hoods  
Rating - Choice fields from 1 to 5  
Comments - text area field  


# For this test use
Django 1.11  
Python 3  
PostgreSQL 9.5  
Twitter Bootstrap v3 (css and Javascript)  
Any other python/javascript libraries that you find relevant  

## Solution
The site is live [here](http://feedback.danleyb2.online)

#### Test Accounts
Admin danleyb2@gmail.com `brian123`    
User danleyb2@outlook.com `brian123`   


