import os

import logging
from datetime import datetime

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class ColumnNotFound(Exception):
    pass


class DataLoader(object):
    def __init__(self, csv_file):

        self.columns = None
        self.rows = []

        abs_file_path = os.path.abspath(csv_file)
        with open(abs_file_path, 'r') as data_file:
            line_no = 0
            data = data_file.readlines()
            date_column = None
            for row in data:
                cells = row.strip().split(',')
                line_no += 1
                if line_no == 1:
                    self.columns = cells
                    date_column = self.find_column_index('Harvesting date')
                    continue

                # datetime object is created from date string for sorting by date to work
                cells[date_column] = datetime.strptime(cells[date_column], '%d/%m/%Y')
                self.rows.append(cells)
        # logger.info(self.__str__())

    def size(self):
        return len(self.rows)

    def avg(self, column):
        total = 0
        count = self.size()
        for row in self.rows:
            try:
                total += float(row[column])
            except ValueError as ex:
                raise Exception('Average requires only Number values')

        return total / float(count)

    def sort(self, column):
        """
        Sort the data in Ascending order by a column index
        :param column:
        :return:
        """

        # list.sort(key=lambda item:item['date'], reverse=True)

        return sorted(self.rows, key=lambda r: r[column])

    def find_column_index(self, title):
        """
        Finds a column index by title
        :param title: The column label
        :type title: str
        :return: The column index if exists
        :rtype: int
        :raises:
        """
        for index, column in enumerate(self.columns):
            # print('{} {}'.format(index, column))
            if column == title:
                return index

        raise ColumnNotFound("Couldn't find column {}, make sure it exists and title is correct".format(title))

    def __str__(self):
        to_str = str(self.columns) + '\n\n' + '\n'.join(str(row) for row in self.rows)
        return to_str
