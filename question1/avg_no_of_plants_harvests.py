from question1.app import DataLoader


def main():
    file_name = "../data/Sample Data for test - Sheet1.csv"
    sample_data = DataLoader(file_name)

    no_of_plants_harvest_column_title = 'No of plants harvest'
    no_of_plants_harvest_column_index = sample_data.find_column_index(no_of_plants_harvest_column_title)

    no_of_plants_harvest_avg = sample_data.avg(no_of_plants_harvest_column_index)

    print(no_of_plants_harvest_avg)

main()