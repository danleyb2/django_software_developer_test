from question1.app import DataLoader


def main():
    file_name = "../data/Sample Data for test - Sheet1.csv"
    sample_data = DataLoader(file_name)

    total_biomass_column_title = 'Total biomass oven Dwt (kg)'
    total_biomass_column_index = sample_data.find_column_index(total_biomass_column_title)
    sorted_sample_data = sample_data.sort(total_biomass_column_index)

    print("The farmers with the highest and lowest Total biomass Fwt (kg)")
    print(sorted_sample_data[-1])
    print(sorted_sample_data[0])



main()