from question1.app import DataLoader


def main():
    file_name = "../data/Sample Data for test - Sheet1.csv"
    sample_data = DataLoader(file_name)

    harvesting_date_column_title = 'Harvesting date'
    harvesting_date_column_index = sample_data.find_column_index(harvesting_date_column_title)
    sorted_sample_data = sample_data.sort(harvesting_date_column_index)

    print("Ordered Harvesting Dates")
    for row in sorted_sample_data:
        print(row)


main()

