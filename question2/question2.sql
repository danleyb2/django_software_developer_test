-- Create sample Data table

CREATE TYPE FARM_MANAGEMENT_CATEGORY AS ENUM ('Low', 'Medium', 'High');
CREATE TABLE IF NOT EXISTS sample_data (
  id                                 SERIAL PRIMARY KEY,
  farmer_name                        CHAR(50),
  farm_management_category           FARM_MANAGEMENT_CATEGORY, --enum Low,Medium,High
  crop                               VARCHAR(100),
  variety                            VARCHAR(50),
  cropping_system                    VARCHAR(20),
  plot_size                          VARCHAR(30),
  spacing                            VARCHAR(30),
  harvesting_date                    DATE,
  sampling_date                      DATE,
  stand_count_at_harvest             INTEGER,
  no_of_plants_harvest               INTEGER,
  total_biomass_fwt                  FLOAT,
  total_grain_fwt                    FLOAT,
  total_stems_shelled_pods_fwt       FLOAT,
  stems_shelled_pods_sample_fwt      FLOAT,
  stems_shelled_pods_sample_oven_dwt INTEGER,
  field_grain_sample_fwt             INTEGER,
  field_grain_sample_oven_dwt        INTEGER,
  total_stems_shelled_pods_oven_dwt  FLOAT,
  total_grain_oven_dwt               FLOAT,
  total_biomass_oven_dwt             FLOAT

);

-- Insert Sample Data

INSERT INTO sample_data (farmer_name, farm_management_category, crop, variety, cropping_system, plot_size, spacing, harvesting_date, sampling_date, stand_count_at_harvest, no_of_plants_harvest, total_biomass_fwt, total_grain_fwt, total_stems_shelled_pods_fwt, stems_shelled_pods_sample_fwt, stems_shelled_pods_sample_oven_dwt, field_grain_sample_fwt, field_grain_sample_oven_dwt, total_stems_shelled_pods_oven_dwt, total_grain_oven_dwt, total_biomass_oven_dwt)
VALUES
  ('Jane Nyambura', 'Low', 'Beans', 'Wairimu', 'Sole', '6x4 m2', '45×10 cm', to_date('12/08/2015', 'DD/MM/YYYY'),
                    to_date('22/10/2015', 'DD/MM/YYYY'), 367, 367, 6, 3.3, 2.7, 124, 108, 1234, 1089, 2.35, 2.91,
                                                                   5.26),
  ('Christopher Macharia', 'Low', 'Maize', 'Kifam', 'Sole', '6x4 m2', '60×5 cm', to_date('21/09/2015',
                                                                                         'DD/MM/YYYY'), to_date(
                               '23/10/2015', 'DD/MM/YYYY'), 222, 222, 1.1, 0, 1.1, 338, 217, 0, 0, 0.71, 0, 0.71),
  ('Mary Mbatia', 'Medium', 'Beans', 'Kifam', 'Sole', '6x4 m2', '65×35 cm', to_date('12/08/2015',
                                                                                    'DD/MM/YYYY'), to_date(
                      '22/10/2015', 'DD/MM/YYYY'), 127, 127, 8.5, 4.6, 3.9, 291, 257, 1046, 953, 3.44, 4.19, 7.64),
  ('Mary Mbatia', 'Medium', 'Beans', 'KAT B2', 'Sole', '6x4 m2', '60×30 cm', to_date('27/07/2015',
                                                                                     'DD/MM/YYYY'), to_date(
                      '23/10/2015', 'DD/MM/YYYY'), 148, 148, 5, 2.6, 2.4, 266, 235, 1001, 906, 2.12, 2.35, 4.47),
  ('Josphat Kangethe', 'Medium', 'Maize', 'Gituru Kanini', 'Sole', '6x4 m2', '50×20cm', to_date('10/7/2015',
                                                                                                'DD/MM/YYYY'), to_date(
                           '23/10/2015', 'DD/MM/YYYY'), 387, 387, 8.4, 4, 4.4, 113, 96, 644, 579, 3.74, 3.6, 7.33),
  ('Joseph Kiarie', 'Medium', 'Beans', 'Kifam', 'Sole', '6x4 m2', '45×20 cm', to_date('13/08/2015',
                                                                                      'DD/MM/YYYY'), to_date(
                        '23/10/2015', 'DD/MM/YYYY'), 280, 280, 4.6, 1.3, 3.3, 163, 143, 565, 506, 2.9, 1.16, 4.06),
  ('Francis Karanja', 'Medium', 'Beans', 'Gituru Kanini', 'Sole', '6x4 m2', '40×30cm', to_date('25/08/2015',
                                                                                               'DD/MM/YYYY'), to_date(
                          '24/10/2015', 'DD/MM/YYYY'), 223, 223, 9.9, 4.9, 5, 332, 284, 1444, 1383, 4.28, 4.69, 8.97),
  ('Francis Karanja', 'Medium', 'Maize', 'Rose Coco', 'Sole', '6x4 m2', '40×20cm', to_date('15/08/2015',
                                                                                           'DD/MM/YYYY'), to_date(
                          '24/10/2015', 'DD/MM/YYYY'), 252, 252, 7, 2.5, 4.5, 388, 336, 1009, 912, 3.9, 2.26, 6.16),
  ('Patrick Mbirwe', 'High', 'Maize', 'Kifam', 'Sole', '6x4 m2', '40×15cm', to_date('13/08/2015',
                                                                                    'DD/MM/YYYY'), to_date(
                         '24/10/2015', 'DD/MM/YYYY'), 264, 264, 7.1, 3.6, 3.5, 234, 202, 1387, 1262, 3.02, 3.28, 6.3),
  ('Wairimu Kanyiri', 'High', 'Beans', 'Kifam', 'Sole', '6x4 m2', '40×20 cm', to_date('13/08/2015',
                                                                                      'DD/MM/YYYY'), to_date(
                          '23/10/2015', 'DD/MM/YYYY'), 247, 247, 5, 2.7, 2.3, 172, 152, 1574, 1419, 2.03, 2.43, 4.47),
  ('George Kahuho', 'Medium', 'Maize', 'Kifam', 'intercrop', '6x4 m2', '60×30cm', to_date('12/08/2015',
                                                                                          'DD/MM/YYYY'), to_date(
                        '24/10/2015', 'DD/MM/YYYY'), 129, 129, 2, 1, 1, 146, 128, 270, 240, 0.88, 0.89, 1.77),
  ('Josphat Kangethe', 'Medium', 'Beans', 'Kifam', 'intercrop', '6x4 m2', '75×50cm',
                       to_date('13/08/2015', 'DD/MM/YYYY'), to_date('22/10/2015', 'DD/MM/YYYY'), 189, 189, 2.3, 0.8,
                                                                                                           1.5, 107,
                                                                                                           93, 314,
                                                                                                           278, 1.3,
                                                                                                           0.71,
                                                                                                           2.01);

-- Find the farmers with the highest and lowest Total biomass Fwt (kg)
CREATE OR REPLACE VIEW highest_and_lowest_total_biomass_fwt AS
  (SELECT *
   FROM sample_data
   ORDER BY total_biomass_fwt DESC
   LIMIT 1)

  UNION ALL

  (SELECT *
   FROM sample_data
   ORDER BY total_biomass_fwt ASC
   LIMIT 1);

-- Sorts the list in ascending order by Harvesting date
CREATE OR REPLACE VIEW ordered_harvesting_dates AS
SELECT *
FROM sample_data
ORDER BY harvesting_date ASC;

-- Find the average No of plants harvest
CREATE OR REPLACE VIEW no_of_plants_harvests_avg AS
  SELECT AVG(no_of_plants_harvest) AS avg_no_of_plants_harvest
  FROM sample_data;

