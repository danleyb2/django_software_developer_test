# Question three (Django test)
Using django create a simple web app that allows a user to login/signup and fill out a feedback form.
The feedback data should only be viewed by an admin/superuser who can then export the data to into a csv.  

## The required fields are
Name (automatically loaded from user object)
Phone number (validation is key here)
Neighbourhood - A drop down with a list of hoods
Rating - Choice fields from 1 to 5
Comments - text area field


## For this test use
Django 1.11
Python 3
PostgreSQL 9.5
Twitter Bootstrap v3 (css and Javascript)
Any other python/javascript libraries that you find relevant


