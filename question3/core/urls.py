from django.conf.urls import url

from .views import account_activation_sent

urlpatterns = [

    url(r'^account_activation_sent/$', account_activation_sent, name='account_activation_sent'),

]
