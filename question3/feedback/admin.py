from django.contrib import admin
from .models import Feedback,Neighbourhood


# Register your models here.
class FeedbackAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Feedback._meta.fields]


admin.site.register(Feedback, FeedbackAdmin)


class NeighbourhoodAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Neighbourhood._meta.fields]


admin.site.register(Neighbourhood, NeighbourhoodAdmin)
