from django.db import models
from core.models import Account
from phonenumber_field.modelfields import PhoneNumberField
from django.core.urlresolvers import reverse

# Create your models here.


'''
Name (automatically loaded from user object)
Phone number (validation is key here)
Neighbourhood - A drop down with a list of hoods
Rating - Choice fields from 1 to 5
Comments - text area field

'''


class Neighbourhood(models.Model):
    name = models.CharField(max_length=50, unique=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Feedback(models.Model):
    RATTING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    name = models.ForeignKey(Account)
    phone = PhoneNumberField(max_length=255, blank=True,
                             help_text='Your phone number including the country code. e.g +254717667590')
    neighbourhood = models.ForeignKey(Neighbourhood, help_text='Where are you from?')
    rating = models.SmallIntegerField(choices=RATTING_CHOICES)
    comment = models.TextField(help_text='Your feedback comment')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name.get_username()

    def get_absolute_url(self):
        return reverse('feedback:feedback-detail', kwargs={'pk': self.pk})
