from feedback.views import FeedbackListView,FeedbackDetailView,index,export
from django.conf.urls import url, include

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^feedback/', include([
        url(r'^$', FeedbackListView.as_view(), name='feedback-list'),
        url(r'^export$', export, name='feedback-export'),
        url(r'^(?P<pk>\d+)$', FeedbackDetailView.as_view(), name='feedback-detail'),
    ])),
]
