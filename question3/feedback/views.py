from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic.list import ListView

from feedback.forms import FeedbackForm
from feedback.models import Feedback


# Create your views here.

def index(request):
    if request.user.is_staff:
        return redirect('/feedback')

    context = {}

    if request.method == "POST":
        if not request.user.is_active:
            raise PermissionDenied

        form = FeedbackForm(request.POST)
        if form.is_valid():
            feedback = form.save(commit=False)

            feedback.name = request.user

            feedback.save()
            return redirect('/')
    else:
        feedback = Feedback.objects.filter(name=request.user)
        if feedback.exists():
            context['feedback'] = feedback.first()
        else:
            form = FeedbackForm()
            context['form'] = form

    return render(request, 'feedback/create.html', context)


class FeedbackListView(ListView):
    template_name = 'feedback/list.html'
    model = Feedback
    context_object_name = 'feedbacks'
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return redirect('/')
        return super(FeedbackListView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        object_list = self.model.objects.filter()
        q = self.request.GET.get('q', None)
        if q:
            object_list = object_list.filter(name__email__icontains=q)

        return object_list.order_by('pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['feedback_count'] = Feedback.objects.count()
        return context


class FeedbackDetailView(DetailView):
    model = Feedback
    context_object_name = 'feedback'
    template_name = 'feedback/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['feedback_count'] = Feedback.objects.count()
        return context


def export(request):
    import datetime
    now = datetime.datetime.now()

    filename = now.strftime("feedbacks_%B_%d_%Y__%H_%M")

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(filename)

    feedback_list = Feedback.objects.all()

    writer = csv.writer(response)
    writer.writerow([
        'id',
        'from',
        'phone',
        'neighbourhood',
        'rating',
        'comment',
        'created_at'
    ])

    try:
        for li in feedback_list:
            writer.writerow([
                li.pk,
                li.name.email,
                li.phone,
                li.neighbourhood,
                li.rating,
                li.comment,
                li.created_at
            ])

    except Exception as e:
        raise

    return response
